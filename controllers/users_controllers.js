const User = require('../models/user');

module.exports.profile = function(req,res){
    return res.render('user_profile', {
        title: "Profile"
    })
}

// render the sign up page
module.exports.signUp = function(req,res){
    return res.render('user_sign_up',{
        title: "Codeial | Sign Up"
    });
}

// render the sign in page
module.exports.signIn = function(req,res){
    return res.render('user_sign_in',{
        title: "Codeial | Sign In"
    });
}

// get the sign up data
module.exports.create = async function(req,res){
    if(req.body.password != req.body.confirm_pass){
        return res.redirect('back');
    }

    const user = await User.create({email: req.body.email, name: req.body.name, password: req.body.password});
    if(user){
        // return res.status(200).json({
        //     message: "Done",
        //     data: user
        // });
        return res.redirect('sign-in');
    }
}

// sign in and create a session for the user
module.exports.createSession = function(req,res){
    // TODO later
}


