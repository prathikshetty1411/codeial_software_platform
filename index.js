const express = require('express');
const cookieParser = require('cookie-parser');
const path = require('path');
const app = express();
const port = 8000;

app.use(express.urlencoded());

app.use(cookieParser());

// initializing the DB
const db = require('./config/mongoose');

// express-ejs-layout
const expressLayouts = require('express-ejs-layouts');
app.use(expressLayouts);

// loading static files
app.use(express.static('./assets'));

// extract style scripts from sub pages into the layout
app.set('layout extractStyles', true);
app.set('layout extractScripts', true);


// use express router
app.use('/', require('./routes'));

// set up the view engine
app.set('view engine', 'ejs');
app.set('views', './views');

app.listen(port, function(err){ 
    if(err){
        console.log(`Error in running the server: ${err}`);
    }
    console.log(`Server is running on port: ${port}`)
});